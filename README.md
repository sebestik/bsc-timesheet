# BSC timesheet excel report #

Java based project for downloading timesheet report via JIRA rest API and writing result to predefined excel, which only contains mandatory data.

## Basic Usage (Specify JIRA credentials and MD rate) ###

* java -jar bsc-timesheet-1.1.1-jar-with-dependencies.jar -r 10000 -cc 02100222 -ac 02 -u koutny -p HESLO

## Development ###

### Requirements ###

* Java 8

### Building ###

Go to folder where ```pom.xml``` is located and run command:
```
mvn clean install assembly:single
```

