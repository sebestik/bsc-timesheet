package cz.bsc.timesheet;

/**
 * Exception signalizujici nenacteni URL (http response status signalizuje neocekavany stav)
 * 
 * @author rsebestik
 */
public class WebResourceException extends Exception {

	private static final long serialVersionUID = 8803872091030612701L;

	public WebResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public WebResourceException(String message) {
		super(message);
	}

	public WebResourceException(Throwable cause) {
		super(cause);
	}
}
