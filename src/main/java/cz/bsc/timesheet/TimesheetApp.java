package cz.bsc.timesheet;

import static java.lang.String.format;
import static java.math.RoundingMode.HALF_UP;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.lang3.StringUtils.capitalize;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import groovy.json.JsonSlurper;

/**
 * App thats downloads timesheet report for given month from JIRA rest API and saves it into pre-prepared excel.
 * 
 * @author rsebestik
 */
public final class TimesheetApp {

	private static final String JIRA_URL_DEFAULT = "https://support.bsc-ideas.com/jira/rest/tempo-timesheets/3/";
	private static final String EMPTY_EXCEL_DEFAULT = "TimeSheetReport_empty.xlsx";

	private static final DateTimeFormatter MONTH_FORMATTER = ofPattern("MM");
	private static final DateTimeFormatter DATE_FORMATTER = ofPattern("d.M.yyyy");
	private static final String QUERY = "worklogs/?username=%s&dateFrom=%s&dateTo=%s";
	private static final Map<String, String> POSSIBLE_ACTIVITIES;
	private static final String POSSIBLE_ACTIVITIES_DESCRIPTION;

	private static WebResourceReader webResourceReader;

	static {
		final Map<String, String> possibleActivities = new HashMap<>(6);
		possibleActivities.put("01", "ANALYSIS");                           
		possibleActivities.put("02", "PROGRAMMING");
		possibleActivities.put("03", "TESTING");
		possibleActivities.put("05", "PROJECT MANAGER");                           
		possibleActivities.put("06", "TECHN.SERVICES");                           
		possibleActivities.put("07", "CONSULTANCY"); 
		POSSIBLE_ACTIVITIES = MapUtils.unmodifiableMap(possibleActivities);

		String possibleActivitiesDesc = "required - KOD CINNOSTI (2-digit code) \n"
				+ "Possibilities: \n";

		for (Entry<String, String> entry : POSSIBLE_ACTIVITIES.entrySet()) {
			possibleActivitiesDesc += entry.getKey() + " (use for " + entry.getValue() + ")\n";
		}
		POSSIBLE_ACTIVITIES_DESCRIPTION = possibleActivitiesDesc;

	}

	/**
	 * JIRA base rest api url
	 */
	private String jiraApiUrl;

	/**
	 * Empty excel file name (with prepared structure of excel needed to be used in BSC)
	 */
	private File emptyExcel;

	/**
	 * JIRA Username
	 */
	private String jiraUsername;

	/**
	 * Price per day
	 */
	private Integer mdRate;

	/**
	 * Analyzed month
	 */
	private YearMonth month;

	/**
	 * Cost center (Nakladove stredisko - aka 'StredNakladPol')
	 */
	private String costCenter;

	/**
	 * Activity code (Kod cinnosti - aka 'EvCisloPol')
	 */
	private String activityCode;

	/**
	 * Constructor
	 */
	private TimesheetApp() {
		//empty
	}

	/**
	 * Main method
	 * @throws ParseException 
	 * @throws WebResourceException 
	 * @throws IOException 
	 * @throws org.apache.commons.cli.ParseException 
	 */
	public static void main(final String[] args) throws IOException, WebResourceException, ParseException, org.apache.commons.cli.ParseException {
		new TimesheetApp().init(args).analyze();
	}

	/**
	 * Analyze timesheet per project
	 * 
	 * @throws IOException
	 * @throws WebResourceException
	 * @throws ParseException
	 */
	private void analyze() throws IOException, WebResourceException, ParseException {
		System.out.println("Analyze start");

		//call JIRA API
		final String searchedWorklogs = webResourceReader.get(jiraApiUrl  + format(QUERY, jiraUsername, month.atDay(1), month.atEndOfMonth()));

		//extract timesheet money per project
		final LinkedMap<String, BigDecimal> moneyPerProject = loadProjectTimesheets(searchedWorklogs);

		//load empty excel
		final Workbook wb = loadExcel();

		//write data
		writeTimesheet(wb, moneyPerProject);

		//save
		saveExcel(wb);
	}

	/**
	 * Init app, params, etc.
	 * 
	 * @param args
	 * @return
	 * @throws org.apache.commons.cli.ParseException
	 */
	private TimesheetApp init(final String[] args) throws org.apache.commons.cli.ParseException {
		final Options options = new Options();
		options.addOption(Option.builder("m").longOpt("month").required(false).hasArg().desc("optional - specify period in 'yyyy-MM' format (defaults to current month if its at the end of month or to previous month if current day is at the beginning of month)").build());
		options.addOption(Option.builder("r").longOpt("rate").required().hasArg().type(Number.class).desc("required - specify MD rate").build());
		options.addOption(Option.builder("u").longOpt("jiraUsername").required().hasArg().desc("required - JIRA login name").build());
		options.addOption(Option.builder("p").longOpt("jiraPassword").required().hasArg().desc("required - JIRA login password").build());
		options.addOption(Option.builder("f").longOpt("file").required(false).hasArg().desc("optional - name of input empty excel file (defaults to 'TimeSheetReport_empty.xlsx')").build());
		options.addOption(Option.builder("a").longOpt("jiraApiUrl").required(false).hasArg().desc("optional - JIRA API url (defaults to 'https://support.bsc-ideas.com/jira/rest/tempo-timesheets/3/')").build());
		options.addOption(Option.builder("cc").longOpt("costCenter").required().hasArg().desc("required - NAKLADOVE STREDISKO").build());
		options.addOption(Option.builder("ac").longOpt("activityCode").required().hasArg().desc(POSSIBLE_ACTIVITIES_DESCRIPTION).build());

		final HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(format("java -jar bsc-timesheet-%s-jar-with-dependencies.jar", this.getClass().getPackage().getImplementationVersion()), options, true);

		final CommandLineParser parser = new DefaultParser();
		final CommandLine cmd = parser.parse(options, args);

		this.month = cmd.hasOption('m') ? YearMonth.parse(cmd.getOptionValue('m')) : getDefaultYearMonth();
		this.emptyExcel = new File(cmd.hasOption('f') ? cmd.getOptionValue('f') : EMPTY_EXCEL_DEFAULT);
		this.mdRate = ((Number) cmd.getParsedOptionValue("r")).intValue();
		this.jiraUsername = cmd.getOptionValue('u');
		this.jiraApiUrl = cmd.hasOption('a') ? cmd.getOptionValue('a') : JIRA_URL_DEFAULT;
		this.costCenter = cmd.getOptionValue("cc");
		this.activityCode = cmd.getOptionValue("ac");

		if (!POSSIBLE_ACTIVITIES.keySet().contains(this.activityCode)) {
			throw new RuntimeException("Illegal activity code, possibilities: " + POSSIBLE_ACTIVITIES.keySet());
		}

		webResourceReader = new WebResourceReader();
		webResourceReader.setCredentials(this.jiraUsername, cmd.getOptionValue('p'));

		return this;
	}

	/**
	 * Load empty data excel
	 * 
	 * @return
	 * @throws EncryptedDocumentException
	 * @throws IOException
	 */
	private Workbook loadExcel() throws EncryptedDocumentException, IOException {
		final FileInputStream fis = new FileInputStream(emptyExcel);
		System.out.println("...empty file found: " + emptyExcel.getAbsolutePath());
		final Workbook wb = WorkbookFactory.create(fis);
		System.out.println("...workbook opened");
		return wb;
	}

	/**
	 * Write data from JIRA
	 * 
	 * @param wb
	 * @param moneyPerProject
	 */
	private void writeTimesheet(Workbook wb, LinkedMap<String, BigDecimal> moneyPerProject) {
		final Sheet sheet = wb.getSheetAt(0);
		System.out.println("...1st sheet found");

		for (int i = 0; i < moneyPerProject.size(); i++) {
			int rowIndex = i + 1;
			if (i < moneyPerProject.size() - 1) {
				//copy pre-prepared empty row - for next project
				((XSSFSheet)sheet).copyRows(rowIndex, rowIndex, rowIndex + 1, new CellCopyPolicy());
			}

			final Row row = sheet.getRow(rowIndex);
			row.getCell(7).setCellValue(moneyPerProject.get(i));//project
			row.getCell(2).setCellValue(moneyPerProject.getValue(i).doubleValue());//money

			row.getCell(1).setCellValue(costCenter);
			row.getCell(8).setCellValue(activityCode);

			//date should be either current date or end of previous month if its done at the beginning of the month
			final String currentDate = getDateForExcel().format(DATE_FORMATTER);
			row.getCell(5).setCellValue(currentDate);
			row.getCell(6).setCellValue(currentDate);
		}
	}

	/**
	 * Build data from json response
	 * 
	 * @param json
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings({"rawtypes"})
	private LinkedMap<String, BigDecimal> loadProjectTimesheets(String json) throws ParseException {
		final Map<String, Integer> secondsPerProject = new HashMap<>();
		final LinkedMap<String, BigDecimal> moneyPerProject = new LinkedMap<>();

		for (Map worklog : parseJSON(json)) {
			final Map issue = (Map) worklog.get("issue");
			final String key = (String) issue.get("key");
			final String project = StringUtils.substringBefore(key, "-");
			Integer seconds = (Integer) worklog.get("timeSpentSeconds");
			if (secondsPerProject.containsKey(project)) {
				seconds = seconds + secondsPerProject.get(project);
			}
			secondsPerProject.put(project, seconds);
		}

		System.out.println("======================================");

		BigDecimal total = BigDecimal.ZERO;
		for (Entry<String, Integer> e : secondsPerProject.entrySet()) {
			final BigDecimal hours = BigDecimal.valueOf(e.getValue()).divide(BigDecimal.valueOf(3600), 5, HALF_UP);
			final BigDecimal money = hours.divide(BigDecimal.valueOf(8), 5, HALF_UP).multiply(BigDecimal.valueOf(mdRate)).setScale(2, HALF_UP);
			moneyPerProject.put(e.getKey(), money);
			total = total.add(money);
		}
		System.out.println("Money per project: " + moneyPerProject);
		System.out.println("======================================");
		System.out.println(format("Total money for %s: %s", month, total));
		System.out.println("======================================");

		return moneyPerProject;
	}

	/**
	 * Parse json
	 * 
	 * @param json
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	private List<Map> parseJSON(String json) throws ParseException {
		System.out.println("parseJSON() START json: " + StringUtils.abbreviate(json, 100));

		List<Map> parsedJSON = (List<Map>) new JsonSlurper().parseText(json);
		System.out.println("parseJSON() END found worklogs: " + parsedJSON.size());

		return parsedJSON;
	}

	/**
	 * Save excel file. It uses, file-name needed by BSC
	 * 
	 * @param wb
	 * @throws IOException
	 */
	private void saveExcel(Workbook wb) throws IOException {
		final String output = FilenameUtils.getFullPath(emptyExcel.getAbsolutePath()) + format("/TimeSheetReport_%s_%s_%s.xlsx", capitalize(jiraUsername), month.format(MONTH_FORMATTER), month.getYear(), Locale.getDefault());
		FileOutputStream fos = new FileOutputStream(output);
		wb.write(fos);
		fos.flush();
		fos.close();
		wb.close();
		System.out.println("======================================");
		System.out.println("File written: " + output);
		System.out.println("======================================");
	}

	/**
	 * If no month is specified in input, this will return month for processing, either current or previous.
	 * @return
	 */
	private YearMonth getDefaultYearMonth() {
		if (LocalDate.now().getDayOfMonth() > 10) {
			//if we are close to end of month -> return current month
			return YearMonth.now();
		} else {
			//if we are at the beginning of month, most probably, the report is for previous month
			return YearMonth.now().minusMonths(1);
		}
	}

	/**
	 * Get date for excel, should be current date if its at the end of
	 * @return
	 */
	private LocalDate getDateForExcel() {
		final LocalDate now = LocalDate.now();
		final LocalDate endOfReportedMonth = month.atEndOfMonth();
		if (now.isAfter(endOfReportedMonth)) {
			//when report at the beginning of next month - previous month last day should be used
			return endOfReportedMonth;
		} else {
			//when report is done before end of month, current date should be used
			return now;
		}
	}
}
