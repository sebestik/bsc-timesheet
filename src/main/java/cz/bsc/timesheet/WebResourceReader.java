package cz.bsc.timesheet;

import static java.lang.String.format;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class WebResourceReader {

	private final Client client = Client.create();

	public void setCredentials(String username, String password) {
		client.addFilter(new HTTPBasicAuthFilter(username, password));
	}

	public String get(String endpointUrl) throws WebResourceException {
		System.out.println(format("get() url: '%s'", endpointUrl));

		WebResource webResource = client.resource(endpointUrl);
		try {
			return webResource.get(String.class);
		} catch (Exception e) {
			throw new WebResourceException("Error get URL: " + endpointUrl, e);
		}
	}
}